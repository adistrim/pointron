import StorageSettings from "$lib/client/products/pointron/settings/data/StorageSettings.svelte";
import SessionSettings from "$lib/client/products/pointron/settings/SessionSettings.svelte";
import WidgetSettings from "$lib/client/products/pointron/settings/WidgetSettings.svelte";
import TrackingSettings from "$lib/client/products/pointron/settings/targets/TrackingSettings.svelte";
import Focus from "$lib/client/products/pointron/focus/Focus.svelte";
import Zen from "$lib/client/products/pointron/focus/zen/Zen.svelte";
import FocusPlayer from "$lib/client/products/pointron/focus/player/FocusPlayer.svelte";
import { ActionType, type IAction } from "$lib/client/types/action.type";
import Journal from "$lib/client/products/pointron/journal/Journal.svelte";
import { PointronEventEnum } from "$lib/client/types/pointron/pointronEvent.enum";
import AddTagModal from "$lib/client/products/pointron/tag/AddTagModal.svelte";
import ImportAppData from "$lib/client/products/pointron/settings/ImportAppData/ImportAppData.svelte";
import TagsList from "$lib/client/products/pointron/settings/tags/TagsList.svelte";
import ConvertToSubGoalModal from "$lib/client/products/pointron/goals/ConvertToSubGoalModal.svelte";
import EditPresetView from "$lib/client/products/pointron/focus/advanced/presets/EditPresetModal.svelte";
import Onboarding from "$lib/client/products/pointron/onboarding/Onboarding.svelte";
import ComposeByEndTimeModal from "$lib/client/products/pointron/focus/advanced/composition/ComposeByEndTimeModal.svelte";
import ComposeModal from "$lib/client/products/pointron/focus/advanced/composition/ComposeModal.svelte";
import ComingSoonView from "$lib/client/elements/ComingSoonView.svelte";
import PresetSaveConfirmationModal from "$lib/client/products/pointron/focus/advanced/presets/PresetSaveConfirmationModal.svelte";
import SessionFinishedModal from "$lib/client/products/pointron/focus/elements/SessionFinishedModal.svelte";
import EditTagModal from "$lib/client/products/pointron/tag/EditTagModal.svelte";
import AnalyticsSettings from "$lib/client/products/pointron/settings/AnalyticsSettings.svelte";
import Think from "$lib/client/products/pointron/focus/Think.svelte";
import BackgroundMusic from "$lib/client/products/pointron/focus/backgroundMusic/BackgroundMusic.svelte";
import { Size } from "$lib/client/types/size.enum";
import { ButtonVariant } from "$lib/client/types/button.type";
import { get } from "svelte/store";
import { Item } from "$lib/client/types/item.enum";
import modalEvent from "$lib/client/components/modal/modal.store";
import {
  toasts,
  confirmationNotification
} from "$lib/client/stores/notification.store";
import { AlertType } from "$lib/client/types/notification.type";
import { generateUID } from "$lib/client/utils/utils";
import CreateGoal from "$lib/client/products/pointron/goals/create/CreateGoal.svelte";
import GoalHomeV2 from "$lib/client/products/pointron/goals/home/GoalHomeV2.svelte";
import FocusItemsModal from "$lib/client/products/pointron/focus/advanced/FocusItemsModal.svelte";
import BreakReminderModal from "$lib/client/products/pointron/focus/elements/BreakReminderModal.svelte";
import PredefinedIntervalNotifierOverlay from "$lib/client/products/pointron/focus/elements/PredefinedIntervalNotifierOverlay.svelte";
import { pointLogStore } from "$lib/client/products/pointron/logs/log.store";
import ControlPanelLogsPane from "$lib/client/products/pointron/logs/ControlPanelLogsPane.svelte";
import SessionLogPage from "$lib/client/products/pointron/logs/logPage/SessionLogPage.svelte";
import ManualLogPane from "$lib/client/products/pointron/logs/manualLog/ManualLogPane.svelte";
import LogsPane from "$lib/client/products/pointron/logs/LogsPane.svelte";
import AnalyticsV2 from "$lib/client/products/pointron/analytics/AnalyticsV2.svelte";
import { Orientation } from "$lib/client/types/direction.enum";
import PresetSettings from "$lib/client/products/pointron/focus/advanced/presets/PresetSettings.svelte";
import { sessionStore } from "$lib/client/products/pointron/focus/session.store";
import { Persistence } from "$lib/client/persistence/persistence";
import { pointronEvents } from "$lib/client/products/pointron/pointron.store";
import { appStore } from "$lib/client/stores/app.store";

const isSessionRunningPreCondition = () => get(sessionStore).isSessionRunning;

export const localActions: IAction[] = [
  {
    action: PointronEventEnum.FULL_SCREEN_FOCUS,
    component: Zen,
    icon: "zen",
    type: ActionType.META_MODAL,
    associatedPlayer: PointronEventEnum.FOCUS_PLAYER,
    modalParams: {
      layout: {
        ignoreSafeArea: true,
        size: Size.full
      }
    }
  },
  {
    action: PointronEventEnum.SESSION_FINISHED,
    component: SessionFinishedModal,
    type: ActionType.META_MODAL,
    modalParams: {
      isDismissable: false,
      layout: {
        size: Size.md,
        orientation: Orientation.Vertical,
        primaryAction: {
          label: "Done",
          callback: () => sessionStore.close()
        }
      }
    }
  },
  {
    action: PointronEventEnum.SAVE_PRESET_MODAL,
    component: PresetSaveConfirmationModal,
    type: ActionType.META_MODAL,
    modalParams: {
      layout: {
        size: Size.xs,
        primaryAction: {
          label: "Save Preset",
          callback: () => sessionStore.saveCurrentCompositionAsPreset()
        },
        secondaryAction: {
          label: "Cancel"
        }
      }
    }
  },
  {
    action: PointronEventEnum.SESSION_LOG_MODAL,
    component: SessionLogPage,
    type: ActionType.META_MODAL,
    modalParams: {
      title: "Session Log",
      layout: {
        size: Size.xl,
        isShowClose: true
      }
    }
  },
  {
    action: PointronEventEnum.COMPOSE_BY_END_TIME_MODAL,
    component: ComposeByEndTimeModal,
    type: ActionType.META_MODAL,
    modalParams: {
      title: "Choose end time",
      layout: {
        secondaryAction: {
          label: "Done"
        }
      }
    }
  },
  {
    action: PointronEventEnum.COMPOSE_TIME_MODAL,
    component: ComposeModal,
    type: ActionType.META_MODAL,
    modalParams: {
      layout: {
        size: Size.lg,
        primaryAction: {
          label: "Proceed"
        },
        secondaryAction: {
          label: "Cancel"
        }
      }
    }
  },
  {
    action: PointronEventEnum.MANUAL_FOCUS_ENTRY_POP,
    component: ManualLogPane,
    get label() {
      return this.modalParams?.title;
    },
    type: ActionType.MODAL,
    modalParams: {
      title: "Manual time entry",
      isShowAsSheet: true,
      layout: {
        size: Size.xl,
        primaryAction: {
          label: "Save entries",
          callback: () => pointLogStore.saveManualLogs()
        },
        secondaryAction: {
          label: "Discard",
          callback: () => {
            setTimeout(() => {
              pointLogStore.reset();
            }, 100);
            return Promise.resolve(true);
          }
        }
      }
    }
  },
  {
    action: PointronEventEnum.EDIT_PRESET,
    component: EditPresetView,
    type: ActionType.MODAL,
    label: "Create a new preset",
    modalParams: {
      title: "Edit Preset",
      layout: {
        size: Size.lg
      }
    }
  },
  {
    action: PointronEventEnum.BREAK_REMINDER,
    component: BreakReminderModal,
    type: ActionType.META_MODAL,
    modalParams: {
      layout: {
        size: Size.lg,
        orientation: Orientation.Vertical,
        secondaryAction: {
          label: "Continue working"
        },
        primaryAction: {
          label: "Take break",
          callback: () => sessionStore.startBreak()
        }
      }
    }
  },
  {
    action: PointronEventEnum.PREDEFINED_INTERVAL_NOTIFIER,
    component: PredefinedIntervalNotifierOverlay,
    type: ActionType.META_MODAL,
    modalParams: {
      isShowOverlay: false,
      layout: {
        size: Size.xs,
        ignoreSafeArea: true
      }
    }
  },
  {
    action: PointronEventEnum.ADD_TAG,
    component: AddTagModal,
    type: ActionType.MODAL,
    label: "Add Tag",
    modalParams: {
      title: "Add Tag",
      layout: {
        size: Size.xs
      }
    }
  },
  {
    action: PointronEventEnum.EDIT_TAG,
    type: ActionType.META_MODAL,
    component: EditTagModal,
    modalParams: {
      layout: {
        size: Size.xs
      }
    }
  },
  {
    action: PointronEventEnum.CONVERT_TO_SUBGOAL,
    component: ConvertToSubGoalModal,
    type: ActionType.META_MODAL
  },
  {
    action: PointronEventEnum.IMPORT_APP_DATA,
    component: ImportAppData
  },
  {
    action: PointronEventEnum.LOGS,
    component: LogsPane,
    isMenuHidden: true,
    get label() {
      return this.modalParams?.title;
    },
    cmdLabel: "See Logs",
    type: ActionType.MODAL,
    icon: "history",
    modalParams: {
      title: "Logs",
      isShowAsSheet: true,
      layout: {
        size: Size.lg
      }
    }
  },
  {
    action: "onboarding",
    component: Onboarding,
    isMenuHidden: true,
    label: "Onboarding",
    type: ActionType.META_PAGE
  },
  {
    action: "cplogs",
    component: ControlPanelLogsPane,
    path: "cp/logs",
    label: "Logs",
    type: ActionType.META_PAGE,
    icon: "history"
  },
  {
    action: PointronEventEnum.TAGS,
    component: TagsList,
    path: "cp/tags",
    label: "Tags",
    icon: "tag",
    type: ActionType.MODAL
  },
  {
    action: "journal",
    component: Journal,
    icon: "journal",
    label: "Journal",
    type: ActionType.PAGE
  },
  {
    action: PointronEventEnum.FOCUS_PLAYER,
    type: ActionType.META,
    component: FocusPlayer
  },
  {
    action: "focus",
    component: Focus,
    icon: "focus",
    type: ActionType.PAGE,
    label: "Focus"
  },
  {
    action: "analytics",
    component: AnalyticsV2,
    type: ActionType.PAGE,
    icon: "chart",
    label: "Analytics"
  },
  {
    action: "goal",
    component: GoalHomeV2,
    icon: "goals",
    type: ActionType.PAGE,
    label: "Goals"
  },
  {
    action: PointronEventEnum.SESSION_SETTINGS_MODAL,
    get cmdLabel() {
      return this.modalParams?.title;
    },
    label: "Session",
    path: "cp/session",
    type: ActionType.MODAL,
    component: SessionSettings,
    modalParams: {
      title: "Session Settings",
      layout: {
        primaryAction: {
          label: "Done"
        }
      }
    }
  },
  {
    action: "alerts",
    cmdLabel: "Alert Settings",
    label: "Alerts",
    path: "cp/alerts",
    icon: "bell",
    type: ActionType.PAGE,
    component: ComingSoonView
  },
  {
    action: "presets",
    label: "Presets",
    path: "cp/presets",
    icon: "folder",
    type: ActionType.MODAL,
    component: PresetSettings
  },

  {
    action: "importexport",
    get cmdLabel() {
      return this.modalParams?.title;
    },
    label: "Import / Export",
    path: "cp/importexport",
    icon: "data",
    type: ActionType.MODAL,
    // isInactive: true,
    component: StorageSettings,
    modalParams: {
      title: "Import / Export data",
      layout: {
        size: Size.lg,
        orientation: Orientation.Horizontal
      }
    }
  },
  {
    action: "widgets",
    label: "Widgets",
    path: "cp/widgets",
    icon: "widget",
    type: ActionType.PAGE,
    isInactive: true,
    component: WidgetSettings
  },
  {
    action: PointronEventEnum.SET_TARGETS,
    get cmdLabel() {
      return this.modalParams?.title;
    },
    label: "Targets",
    path: "cp/targets",
    icon: "fire",
    type: ActionType.MODAL,
    component: TrackingSettings,
    modalParams: {
      title: "Target Settings"
    }
  },
  {
    action: "analytics-settings",
    get cmdLabel() {
      return this.modalParams?.title;
    },
    label: "Analytics",
    path: "cp/analytic-settings",
    icon: "chart",
    type: ActionType.MODAL,
    component: AnalyticsSettings,
    modalParams: {
      title: "Analytics Settings"
    }
  },
  {
    action: "test",
    label: "Test",
    path: "test",
    type: ActionType.META_PAGE
  },
  {
    action: PointronEventEnum.BACKGROUND_MUSIC,
    label: "Background music",
    type: ActionType.MODAL,
    cmdBarPreCondition: isSessionRunningPreCondition,
    component: BackgroundMusic,
    modalParams: {
      layout: {
        size: Size.lg,
        primaryAction: {
          label: "Done"
        }
      }
    }
  },
  {
    action: "finishSession",
    label: "Finish the session",
    fn: sessionStore.finishSession,
    cmdBarPreCondition: isSessionRunningPreCondition,
    type: ActionType.FUNCTION
  },
  {
    action: "pinAGoal",
    label: "Pin a goal to quick focus",
    type: ActionType.SEARCH_CMD,
    searchActionParams: {
      searchItemType: Item.PointGoal,
      itemLabel: "goal",
      callback: (id: string, label?: string) => {
        console.log("search action selected id:", { id });
        new Persistence().update({ isPinnedForQuickStart: true, id });
        toasts.trigger({
          title: "Goal: " + label,
          message: "Pinned to quick focus",
          actionText: "View",
          type: AlertType.SUCCESS,
          id: generateUID(),
          callback: () => {
            appStore.gotoPath("focus");
          }
        });
      }
    }
  },
  {
    action: PointronEventEnum.ABANDON_SESSION,
    label: "Abandon the current session",
    fn: sessionStore.close,
    type: ActionType.CONFIRMATION,
    cmdBarPreCondition: isSessionRunningPreCondition,
    confirmation: {
      title: "Abandon focus session",
      message: "Are you sure you want to abandon this focus session?",
      confirmAction: {
        label: "Abandon",
        variant: ButtonVariant.DANGER,
        callback: () => {
          return Promise.resolve(sessionStore.close());
        }
      }
    }
  },
  {
    action: PointronEventEnum.CREATE_EDIT_GOAL,
    get label() {
      return this.modalParams?.title;
    },
    type: ActionType.MODAL,
    component: CreateGoal,
    modalParams: {
      title: "Create a new goal",
      isShowAsSheet: true,
      layout: {
        size: Size.lg
      }
    }
  },
  {
    action: PointronEventEnum.THINK_MODE,
    label: "Think mode",
    icon: "think",
    type: ActionType.MODAL,
    cmdBarPreCondition: isSessionRunningPreCondition,
    component: Think,
    modalParams: {
      layout: {
        size: Size.full,
        ignoreSafeArea: true
      }
    }
  },
  {
    action: PointronEventEnum.DELETE_SESSION,
    type: ActionType.FUNCTION,
    fn: async (params: any) => {
      confirmationNotification.notify({
        title: "Delete session",
        message: "Are you sure you want to delete this session log?",
        confirmAction: {
          label: "Delete",
          icon: "trash",
          variant: ButtonVariant.DANGER,
          callback: async () => {
            const response = await pointLogStore.deleteLog(params.id);
            if (response) {
              toasts.success("Session log deleted successfully");
              pointronEvents.notify(PointronEventEnum.REFRESH_LOGS);
            } else {
              toasts.error("Failed to delete session log");
            }
            modalEvent.hideSpecific(PointronEventEnum.SESSION_LOG_MODAL);
          }
        }
      });
    }
  },
  {
    action: PointronEventEnum.SHOW_FOCUSITEMS_MODAL,
    type: ActionType.MODAL,
    component: FocusItemsModal,
    modalParams: {
      title: "Focus Items",
      isShowAsSheet: false,
      layout: {
        size: Size.lg,
        secondaryAction: {
          label: "Done"
        }
      }
    }
  }
];
